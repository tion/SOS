#include <System.h>


void main(void) {
    //use Test's setup
    System_preSetup();
    System_setup();
    System_postSetup();

    System_start();
}
