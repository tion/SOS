-heap 0x0000
-stack 0x1000

MEMORY {
/*  LL2(RWX):   org = 0x00800000, len = 0x00080000 */
	LL2(RWX):   org = 0x00800000, len = 0x00040000

	//stack @ tail 4KB of LL2
	STACK(RW):	org = 0x0087F000, len = 0x00001000

	#if 0
	GL2_0(RWX): org = 0x10800000, len = 0x00080000
	GL2_1(RWX): org = 0x11800000, len = 0x00080000
	GL2_2(RWX): org = 0x12800000, len = 0x00080000
	GL2_3(RWX): org = 0x13800000, len = 0x00080000
	GL2_4(RWX): org = 0x14800000, len = 0x00080000
	GL2_5(RWX): org = 0x15800000, len = 0x00080000
	GL2_6(RWX): org = 0x16800000, len = 0x00080000
	GL2_7(RWX): org = 0x17800000, len = 0x00080000
	#else
	GL2(RWX):   org = 0x10840000, len = 0x0003F000
	#endif

	MSM (RWX):	org = 0x0C000000, len = 0x00400000

/*  OSR (RW) :	org = 0x10000000, len = 0x08000000
	SL3 (RW) :  org = 0x10810000, len = 0x00070000 */

    /* HSM (RW) :	org = 0x1C000000, len = 0x00400000, tail 1M for shared use.
    SHM (RW) :	org = 0x2C000000, len = 0x00200000 */

    PSD (RW) :	org = 0x80000000, len = 0x20000000
	PIX (RX) :	org = 0xA0000000, len = 0x04000000
	PID (RW) :	org = 0xB0000000, len = 0x0C000000
    SHX (RX) :	org = 0xC0000000, len = 0x02000000
	SHD (RW) :	org = 0xD0000000, len = 0x08000000
}

SECTIONS {
	.data.knl			> LL2
	.neardata			> LL2
	.text.knl			> LL2

	.bss				> LL2
	.switch				> LL2
	.far				> LL2
	.sysmem				> LL2

	.data.daemon		> MSM
	.text.init			> MSM
	.text.daemon		> MSM
	.text.aid			> MSM
	.text				> MSM

	.cinit				> MSM

	.far.buffer:Uart	> MSM
	.far.buffer:Console	> MSM


	.far.buffer:*		> GL2
	.far.descriptor:*	> GL2
	.const.firmware		> MSM
	.const				> MSM
	.cio				> MSM

    /* EABI sections */
    .binit         		>  LL2
    .init_array    		>  LL2
    .neardata      		>  LL2
    .fardata       		>  MSM
    .rodata        		>  LL2
    .c6xabi.exidx  		>  LL2
    .c6xabi.extab  		>  LL2

	.stack				> STACK
}
