#ifndef QMSS_FW_H_
#define QMSS_FW_H_

#ifdef __cplusplus
extern "C" {
#endif

#if defined(DEVICE_K2H) || \
    defined(DEVICE_K2K) || \
    defined(DEVICE_K2E) || \
    defined(DEVICE_K2L) || \
    defined(DEVICE_K2G) || \
    defined(SOC_K2G)    || \
    defined(SOC_K2H)    || \
    defined(SOC_K2K)    || \
    defined(SOC_K2E)    || \
    defined(SOC_K2L)

/* Firmware for "Keystone 2" */
#include "V1/acc16_le_bin.h"
#include "V1/acc16_be_bin.h"
#include "V1/acc32_le_bin.h"
#include "V1/acc32_be_bin.h"
#include "V1/acc48_le_bin.h"
#include "V1/acc48_be_bin.h"
#include "V1/qos_le_bin.h"
#include "V1/qos_be_bin.h"
#include "V1/qos_sched_le_bin.h"
#include "V1/qos_sched_be_bin.h"
#include "V1/qos_sched_wide_le_bin.h"
#include "V1/qos_sched_wide_be_bin.h"
#include "V1/qos_sched_drop_sched_le_bin.h"
#include "V1/qos_sched_drop_sched_be_bin.h"
#include "V1/sriortr_le_bin.h"
#include "V1/sriortr_be_bin.h"

#elif defined(DEVICE_C6678) || \
      defined(DEVICE_C6657) || \
      defined(SOC_C6678)    || \
      defined(SOC_C6657)

/* Firmware for "Keystone 1" */
#include "V0/acc16_le_bin.h"
#include "V0/acc16_be_bin.h"
#include "V0/acc32_le_bin.h"
#include "V0/acc32_be_bin.h"
#include "V0/acc48_le_bin.h"
#include "V0/acc48_be_bin.h"
#include "V0/qos_le_bin.h"
#include "V0/qos_be_bin.h"
#include "V0/qos_sched_le_bin.h"
#include "V0/qos_sched_be_bin.h"
#include "V0/qos_sched_wide_le_bin.h"
#include "V0/qos_sched_wide_be_bin.h"
#include "V0/qos_sched_drop_sched_le_bin.h"
#include "V0/qos_sched_drop_sched_be_bin.h"
#include "V0/sriortr_le_bin.h"
#include "V0/sriortr_be_bin.h"

#else
#error Device does not have supported firmware
#endif

/** @addtogroup QMSS_LLD_DATASTRUCT
@{ 
*/        

/** 
 * @brief PDSP Firmware symbols.
 */
/** @brief 32 channel high priority accumulation little endian firmware */
extern const unsigned int __firmware(pa) acc32_le[];
/** @brief 32 channel high priority accumulation big endian firmware */
extern const unsigned int __firmware(pa) acc32_be[];
/** @brief 16 channel low priority accumulation little endian firmware */
extern const unsigned int __firmware(pa) acc16_le[];
/** @brief 16 channel low priority accumulation big endian firmware */
extern const unsigned int __firmware(pa) acc16_be[];
/** @brief 48 channel combined high and low priority accumulation little endian firmware */
extern const unsigned int __firmware(pa) acc48_le[];
/** @brief 48 channel combined high and low priority accumulation big endian firmware */
extern const unsigned int __firmware(pa) acc48_be[];
/** @brief QoS little endian firmware */
extern const unsigned int __firmware(pa) qos_le[];
/** @brief QoS big endian firmware */
extern const unsigned int __firmware(pa) qos_be[];
/** @brief QoS scheduler little endian firmware */
extern const unsigned int __firmware(pa) qos_sched_le[];
/** @brief QoS scheduler big endian firmware */
extern const unsigned int __firmware(pa) qos_sched_be[];
/** @brief QoS scheduler with drop scheduler little endian firmware */
extern const unsigned int __firmware(pa) qos_sched_drop_sched_le[];
/** @brief QoS scheduler with drop scheduler big endian firmware */
extern const unsigned int __firmware(pa) qos_sched_drop_sched_be[];
/** @brief wide QoS scheduler little endian firmware */
extern const unsigned int __firmware(pa) qos_sched_wide_le[];
/** @brief wide QoS scheduler big endian firmware */
extern const unsigned int __firmware(pa) qos_sched_wide_be[];
/** @brief SRIO router/switch - little endian */
extern const unsigned int __firmware(pa) sriortr_le[];
/** @brief SRIO router/switch - big endian */
extern const unsigned int __firmware(pa) sriortr_be[];

/** 
@} 
*/

#ifdef __cplusplus
}
#endif

#endif /* QMSS_FW_H_ */

