/*
 * Psc.c
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */
#include <Hw/Psc.h>

#include <ti/csl/csl_pscAux.h>


Psc_Status
Psc_moduleDisable(
    Uint16 pwrDmnNum,
    Uint16 moduleNum
){
    /* disable the clocks*/
    CSL_PSC_setModuleNextState (moduleNum, PSC_MODSTATE_SWRSTDISABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition(pwrDmnNum);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone(pwrDmnNum));

    return (Psc_Status) CSL_PSC_getModuleState(moduleNum);
}

Psc_Status
Psc_moduleEnable(
    Uint16 pwrDmnNum,
    Uint16 moduleNum
){
    if (CSL_PSC_getPowerDomainState(pwrDmnNum) != PSC_PDSTATE_ON) {
        /* Set Power domain to ON */
        CSL_PSC_enablePowerDomain(pwrDmnNum);
        /* Start the state transition */
        CSL_PSC_startStateTransition (pwrDmnNum);
        /* Wait until the state transition process is completed. */
        while (!CSL_PSC_isStateTransitionDone(pwrDmnNum));
    }

    /* Enable the clocks too*/
    CSL_PSC_setModuleNextState (moduleNum, PSC_MODSTATE_ENABLE);
    /* Start the state transition */
    CSL_PSC_startStateTransition (pwrDmnNum);
    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone(pwrDmnNum));

    /* Return PSC status */
    if ((CSL_PSC_getPowerDomainState(pwrDmnNum) == PSC_PDSTATE_ON) &&
        (CSL_PSC_getModuleState(moduleNum) == PSC_MODSTATE_ENABLE)) {
        return Psc_SOK;/*Ready for use */}
    else {/*Return error */
        return Psc_FAILED; }
}

Psc_Status
PSC_powerDomainDisable(
    Uint16 pwrDmnNum
){
    /* Set Power domain to OFF */
    CSL_PSC_disablePowerDomain (pwrDmnNum);
    /* Start the state transition */
    CSL_PSC_startStateTransition (pwrDmnNum);
    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone(pwrDmnNum));

    /* Return PSC status */
    return (Psc_Status) CSL_PSC_getPowerDomainState(pwrDmnNum);
}
