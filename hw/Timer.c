/*
 * Timer.c
 *
 *  Created on: Oct 15, 2018
 *      Author: ax
 */
#include <Timer.h>


void nop(Uint32 cycle) {
    Timestamp td = Timer_getStamp() + cycle;
    while (td > Timer_getStamp());
}
