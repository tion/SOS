/*
 * Netcp.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_NETCP_H_
#define INCLUDE_HW_NETCP_H_

#include <Type.h>
#include <Hw/Qmss.h>


#define Netcp_BUFFER_SIZE   (1024 + 512)//OOB & pad for > MTU

#define Netcp_PS_DATA_WORD  4

#define Netcp_TX_THRESHOLD  5

#define Netcp_TXBD_CNT      8//32
#define Netcp_RXBD_CNT      32//96
#define Netcp_BDC_ALL       (Netcp_TXBD_CNT + Netcp_RXBD_CNT)


typedef enum {
    Netcp_SOK = 0,
    Netcp_PWR_FAIL,
    Netcp_PA_PDSP_FAULT,
    Netcp_
} Netcp_Status;


typedef enum {
    Netcp_PORTA = 0,
    Netcp_PORTB,
    /* MAY HAVE C,D,E... */
    Netcp_PORT_ALL,
    Netcp_CPSW_PORT_ALL,
    Netcp_PORT_INVALID
} Netcp_Port;


typedef enum {
    Netcp_QPDSP0  = QMSS_PASS_QUEUE_BASE,
    Netcp_QPDSP1,
    Netcp_QPDSP2,
    Netcp_QPDSP3,
    Netcp_QPDSP4,
    Netcp_QPDSP5,
    Netcp_QSA0,
    Netcp_QSA1,
    Netcp_QPSSW,

    Netcp_QUEUE_USE = Queue_Netcp_SLICE,
    Netcp_FDQ,
    Netcp_RXQ,
    Netcp_TCQ,
    Netcp_Q_MAX
} Netcp_Queue;


typedef struct {
    uint8_t     mac0[8];
    uint8_t     mac1[6];
    uint8_t     rxQnumH;
    uint8_t     rxQnumL;
}* PaCfg, paConfig;

#define PA_MAGIC_ID                         0x0CEC11E0


//////// API ////////
extern
Netcp_Status __init
Netcp_init(
    Bits16   portMask,
    Size     mtu,
    Loopback loopback
);

extern
void __init
Netcp_setup(
    void
);

#endif /* INCLUDE_HW_NETCP_H_ */
