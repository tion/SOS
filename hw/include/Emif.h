/*
 * Emif.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef HW_EMIF_H_
#define HW_EMIF_H_

#include <Type.h>


#define NOR_BASE                0x70000000
#define NOR_DATA_BITS           16
#define NOR_SIZE                0x08000000  //128MB
#define SECTOR_SIZE             0x00020000  //128KB
#define RD_PAGE_SZ              0x00000020  //32
#define ADDR_SPACE_SIZE         0x02000000  //32MB

typedef enum {
    EMIF_SOK = 0,
    EMIF_OP_TIMEOUT,
    EMIF_INVALID_ARGS,
    NOR_INCORRECT_ID,
    //TODO add emif error here!
    EMIF_ERROR
} EMIF_Status;

#if NOR_DATA_BITS == 8
typedef Uint8   NorWord;
#elif  NOR_DATA_BITS == 16
typedef Uint16  NorWord;
#else
#error incorrect NOR_DATA_BITS & NorWord, may not supported.
#endif


#if 0
typedef struct {
    struct {
        Uint32   size : 20;
        Uint32   blocks: 12;
    } perSz[3];
}* NOR_BlkInf, NOR_BlockInfo;
extern NOR_BlkInf NOR_open(void);
#endif

extern
EMIF_Status
Nor_erase(
    Uint32   offset,
    Uint32   nbytes
);

extern
EMIF_Status
Nor_read(
    NorWord* to,
    Uint32   offset,
    Uint32   nbytes
);

extern EMIF_Status
Nor_write(
    Uint32   offset,
    const NorWord* from,
    Uint32   nbytes
);

//Controller init., called by boot loader once
extern void __init Emif16_init(void);
extern void __init Emif4f_init(void);

#endif /* HW_EMIF_H_ */
