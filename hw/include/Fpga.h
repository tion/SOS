/*
 * Fpga.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef HW_FPGA_H_
#define HW_FPGA_H_

#include <Type.h>


#define TO_STEP 500//ns
#define RS422_OK 0
#define RS422_TO 1//time out

/* ---------------------RS422-- user API -------------------------- */
static inline int   /* @return : 0 - 成功； others - 失败*/
fpgaUartInit(       /* @function : 打开并初始化RS422接口 */
    unsigned channel/* @input ： FPGA 422通道号 */
){return 0;}
//FIXME 为确保发送数据时序连续性，接口调用期间核中断会被屏蔽
//XXX 基于结果的确定性及资源的不确定性考虑，接口不使用EDMA外部触发，发送接口会阻塞直到FPGA完全接受发送数据或发送超时
//TODO null
int                 /* @return: 0 - 成功； 1 - 超时； -1 - 失败 */
fpgaUartSend(       /* @function: 通过EMIF向FPGA的RS422发送数据 */
    uint8_t channel,   /* @input ： 发送通道 */
    uint8_t* pByte,    /* @input : 待发送的数据起始地址 */
    unsigned len,   /* @input : 发送长度 */
    int timeOut     /* @input : 超时，步进为500ns */
);
//FIXME 即使有更多的可接收数据，在超时时也会返回并返回超时；即使未超时，一旦FPGA指示无更多接收到的数据，接口调用立即返回；
//XXX 基于EDMA及中断资源的不确定性，接口未使用EDMA外部触发背景传输，FPGA获取的RS422数据仅在接口调用时进行数据接收，可能的因FPGA接收区溢出导致的丟数及数据的失效性，需要用户自行把握；
//TODO 超时仅指示在指定的时间内无任何数据，而一旦在超时时间内获得数据可接收状态，便接收数据直到无更多数据；【OR】 接口阻塞直到超时发生，超时返回仅指示在指定的时间内无任何数据；
int                     /* @return: 0 - 成功； 1 - 超时； -1 - 失败 */
fpgaUartRcv(            /* @function: 通过EMIF从FPGA的RS422接收数据 */
    unsigned channel,   /* @input ： 接收通道 */
    uint8_t* pByte,     /* @output : 接收到的数据起始地址 */
    unsigned* pByteLen, /* @output : 接收到的数据的长度 */
    int timeOut         /* @input : 超时，步进为500ns */
);


/* ---------------------温度传感器 用户 API -------------------------- */
typedef enum {
    TEMP_CH0_DSP0 = 0,
    TEMP_CH1_DSP1,
    TEMP_CH2_DSP2,
    TEMP_CH3_DSP3,
    TEMP_CH4_SRIO_SWITCH,
    TEMP_CH_MAX
} TemperatureSensorChannel;
static unsigned         /* @return: 0 - 成功; -1 - 失败 */
adt7461Init(            /* @function : 打开并初始化温度采集芯片接口 */
    unsigned channel    /* @input : 操作的通道 @see TemperatureSensorChannel */
){
    return 0;
}
//FIXME 直接返回温度传感器数值
//TODO 返回转换为整数的摄氏温度值 | ？浮点温度值（output类型更改为float）
unsigned                /* @return : 0 - 成功； 其他 - 失败 */
adt7461ReadLocalTemp(   /* @function : 获取温度值 */
    unsigned channel,   /* @input : 获取的温度的通道/对象，@see TemperatureSensorChannel */
    int * temperature   /* @output : 温度值存储地址 */
);





#endif /* HW_FPGA_H_ */
