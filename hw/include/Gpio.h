/*
 *Gpio.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_GPIO_H_
#define INCLUDE_HW_GPIO_H_

#include <Type.h>

typedef enum {
   Gpio_NONE    = 0x00,
   Gpio_P0      = 0x0001,
   Gpio_P1      = 0x0002,
   Gpio_P2      = 0x0004,
   Gpio_P3      = 0x0008,
   Gpio_P4      = 0x0010,
   Gpio_P5      = 0x0020,
   Gpio_P6      = 0x0040,
   Gpio_P7      = 0x0080,
   Gpio_P8      = 0x0100,
   Gpio_P9      = 0x0200,
   Gpio_P10     = 0x0400,
   Gpio_P11     = 0x0800,
   Gpio_P12     = 0x1000,
   Gpio_P13     = 0x2000,
   Gpio_P14     = 0x4000,
   Gpio_P15     = 0x8000,
   Gpio_ALL_PIN = 0xFFFF
} Gpio_Pin;

typedef enum {
    GP_SET                  = 0x0001,
    GP_CLEAR                = 0x0000,
    GP_DIR_IN               = 0x0000,
    GP_DIR_OUT              = 0x0001,
    GP_SET_RAISE_EDGE_INT   = 0x0004,
    GP_CLEAR_RAISE_EDGE_INT = 0x0000,
    GP_SET_FALL_EDGE_INT    = 0x0008,
    GP_CLEAR_FALL_EDGE_INT  = 0x0000
} Gpio_Attr;


extern
void
Gpio_set(
    Bits16 pins
);

extern
void
Gpio_clear(
    Bits16 pins
);

extern
Bits16
Gpio_getInStatus(
    Bits16 pins
);

extern
Bits16
Gpio_getOutStatus(
    Bits16 pins
);

extern
void
Gpio_setDirIn(
    Bits16 pins
);

extern
void
Gpio_setDirOut(
    Bits16 pins
);

extern
void __init
Gpio_pinSetup(
    Bits16 pins,
    Bits16 attr //Gpio_Attr
);

#endif /* INCLUDE_HW_GPIO_H_ */
