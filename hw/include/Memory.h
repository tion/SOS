/*
 * Memory.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_MEMORY_H_
#define INCLUDE_HW_MEMORY_H_



/* some of bellows can be modified in mcbl.conf during curing progress. */
#define EMIF16_BOOT_BASE        0x70000000
#define PRIVATE_MEM_BASE        0x00800000
#define PRIVATE_MEM_SIZE        0x00080000
#define SHARED_MEM_BASE         0x0C000000
#define SHARED_MEM_SIZE         0x00200000

#if defined(_DBG) && defined(BUILD_CURING_TOOL)
#define BOOT_BASE               SHARED_MEM_BASE
#else
#define BOOT_BASE               EMIF16_BOOT_BASE
#endif


#define FLASH_CURING_SIZE       0x00100000
#define BOOT_LOADER_SIZE        0x00020000  //erase block size.
#define MCBL_TLB_SIZE           0x00000080
#define USR_CURING_SIZE         (FLASH_CURING_SIZE - BOOT_LOADER_SIZE)


#define BOOT0_SZIE              0x00000040
#define LOADER_LOAD_SZ          0x300
#define XSZ                     4
#define LOADER_SZ               (LOADER_LOAD_SZ - XSZ)//limt loader size for ox300
#define BOOT_SIZE               (BOOT_LOADER_SIZE - 0x300)
#define BOOT_RUN                (BOOT_BASE + BOOT0_SZIE)
#define BOOTR_SIZE              (BOOT_SIZE - BOOT0_SZIE)


#define LOADER_LOAD             (BOOT_BASE + BOOT_SIZE)
#define LOADER_RUN              (SHARED_MEM_BASE + SHARED_MEM_SIZE - LOADER_LOAD_SZ)
#define XST                     (SHARED_MEM_BASE + SHARED_MEM_SIZE - XSZ)


#define BL_STACK                (PRIVATE_MEM_BASE + PRIVATE_MEM_SIZE - BL_STACK_SIZE)
#define BL_STACK_SIZE           0x100


#define MCBL_TLB_BASE           (BOOT_BASE + BOOT_LOADER_SIZE)

#define UIMG_BASE               (MCBL_TLB_BASE + MCBL_TLB_SIZE)
#define UIMG_SIZE               (FLASH_CURING_SIZE - BOOT_LOADER_SIZE - MCBL_TLB_SIZE)


#define CURING_TOOL_SIZE        0x00016000
#define CURING_RUN              (SHARED_MEM_BASE + SHARED_MEM_SIZE - CURING_TOOL_SIZE)


/* descriptors */
#define Qmss_FSZ16_DESC_NUM     0
#define Qmss_FSZ32_DESC_NUM     0
#define Qmss_FSZ48_DESC_NUM     0
#define Qmss_FSZ64_DESC_NUM     128
#define Qmss_FSZ128_DESC_NUM    0

#define Qmss_FSZ32_DESC_NUM     0



#ifndef _LINK_
#include <c6x.h>
#include <Type.h>

extern char __descriptor(w4)  hostW4Desc[][16];
extern char __descriptor(w8)  hostW8Desc[][32];
extern char __descriptor(w12) hostW12Desc[][32];
extern char __descriptor(w16) hostW16Desc[][64];
extern char __descriptor(w32) hostW32Desc[][128];


static inline
PtrVal
Memory_core2peripheral(
    const PtrVal addr
){
    return (addr < 0x1000000)  /* local address */
            ? (addr + (0x10000000 | DNUM * 0x1000000))
            : (((addr >= 0x2C000000) && (addr < 0x30000000))/* msmc remaped */
                    ? (addr & 0x0FFFFFFF) : addr);
}

static inline
PtrVal
Memory_peripheral2core(
    const PtrVal addr
){
    return ((addr >= 0x0C000000) && (addr < 0x10000000))
            ? addr | 0x20000000
                    :addr;
}

#endif

#endif /* INCLUDE_HW_MEMORY_H_ */
