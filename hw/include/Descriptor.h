/*
 * Descriptor.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_DESCRIPTOR_H_
#define INCLUDE_HW_DESCRIPTOR_H_

#include <Type.h>

typedef Uint32 Descriptor;

typedef enum {
    Desc_SIZE16 = 0,
    Desc_SIZE32,
    Desc_SIZE48,
    Desc_SIZE64,
    Srio_DescSize = Desc_SIZE64,
    Netcp_DescSize = Desc_SIZE64,
    Desc_SIZE80,
    Desc_SIZE96,
    Desc_SIZE112,
    Desc_SIZE128,
    Desc_SIZE144,
    Desc_SIZE160,
    Desc_SIZE176,
    Desc_SIZE192,
    Desc_SIZE208,
    Desc_SIZE224,
    Desc_SIZE240,
    Desc_SIZE256,
    Desc_HOST_FETCH_SIZE_CNT
} DescSize;
//convert between DescDescSize & actual size
#define Desc_size(fsz)      ((((Uint16) fsz) + 1) << 4)
#define Desc_fsize(sz)      ((((Uint16)  sz) >> 4) - 1)

//descriptor used by receive flow
#define Cpdma_HOST_DESCRIPTOR       1
#define Cpdma_MONO_DESCRIPTOR       2

//for descriptor type id
typedef enum {
    Descriptor_HOST_PACKET  = 0,
    Descriptor_MONOLITHIC_PACKET = 2
} DescTid;

#define Descriptor_PS_IN_DESCRIPTOR 0
#define Descriptor_PS_IN_SOP        1

/*******************************************************************/
/* Define the bit and word layouts for the Host Packet Descriptor. */
/* For a Host Packet, this is used for the first descriptor only.  */
/*******************************************************************/
typedef struct {
    #if (ENDIAN == _BE)
    /* word 0 */
    Uint32 type_id         : 2;  //always 0x0 (Host Packet ID)
    Uint32 packet_type     : 5;
    Uint32 reserved_w0     : 2;
    Uint32 ps_reg_loc      : 1;  //0=PS words in desc, 1=PS words in SOP buff
    Uint32 packet_length   : 22; //in bytes (4M - 1 max)
    /* word 1 */
    Uint32 src_tag_hi      : 8;
    Uint32 src_tag_lo      : 8;
    Uint32 dest_tag_hi     : 8;
    Uint32 dest_tag_lo     : 8;
    /* word 2 */
    Uint32 epib            : 1;  //1=extended packet info block is present
    Uint32 reserved_w2     : 1;
    Uint32 psv_word_count  : 6;  //number of 32-bit PS data words
    Uint32 err_flags       : 4;
    Uint32 ps_flags        : 4;
    Uint32 return_policy   : 1;  //0=linked packet goes to pkt_return_qnum,
                                 //1=each descriptor goes to pkt_return_qnum
    Uint32 ret_push_policy : 1;  //0=return to queue tail, 1=queue head
    Uint32 pkt_return_qmgr : 2;
    Uint32 pkt_return_qnum : 12;
    /* word 3 */
    Uint32 reserved_w3     : 10;
    Uint32 buffer_len      : 22;
    /* word 4 */
    Uint32 buffer_ptr;
    /* word 5 */
    Uint32 next_desc_ptr;
    /* word 6 */
    Uint32 orig_buff0_pool : 4;
    Uint32 orig_buff0_refc : 6;
    Uint32 orig_buff0_len  : 22;
    /* word 7 */
    Uint32 orig_buff0_ptr;
    #else
    /* word 0 : [Descriptor info] - type, packet type, protocol specific region location, packet length*/
    Uint32    packet_length   : 22; //in bytes (4M - 1 max)
    Uint32    ps_reg_loc      : 1;  //0=PS words in desc, 1=PS words in SOP buff
    Uint32    reserved_w0     : 2;
    Uint32    packet_type     : 5;
    Uint32    type_id         : 2;  //always 0x0 (Host Packet ID)
    /* word 1 : src/dst [tag info] */
    Uint32    dest_tag_lo     : 8;
    Uint32    dest_tag_hi     : 8;
    Uint32    src_tag_lo      : 8;
    Uint32    src_tag_hi      : 8;
    /* word 2 : packet info. */
    Uint32    pkt_return_qnum : 12;
    Uint32    pkt_return_qmgr : 2;
    Uint32    ret_push_policy : 1;  //0=return to queue tail, 1=queue head
    Uint32    return_policy   : 1;  //0=linked packet goes to pkt_return_qnum,
                                      //1=each descriptor goes to pkt_return_qnum
    Uint32    ps_flags        : 4;
    Uint32    err_flags       : 4;
    Uint32    psv_word_count  : 6;  //number of 32-bit PS data words
    Uint32    reserved_w2     : 1;
    Uint32    epib            : 1;  //1=extended packet info block is present
    /* word 3 : Number of valid data bytes in the buffer */
    Uint32    buffer_len      : 22;
    Uint32    reserved_w3     : 10;
    /* word 4 : Byte aligned memory address of the buffer associated with this descriptor */
    Uint32    buffer_ptr;
    /* word 5 : chain to next */
    Uint32    next_desc_ptr;
    /* word 6 : Completion tag, original buffer size*/
    Uint32    orig_buff0_len  : 22;
    Uint32    orig_buff0_refc : 6;
    Uint32    orig_buff0_pool : 4;
    /* word 7 : Original buffer pointer */
    Uint32    orig_buff0_ptr;
    #endif
    #ifdef USE_EPIB_WORD
    /** Optional EPIB word0 */
    Uint32    timeStamp;
    /** Optional EPIB word1 */
    Uint32    softwareInfo0;
    /** Optional EPIB word2 */
    Uint32    softwareInfo1;
    /** Optional EPIB word3 */
    Uint32    softwareInfo2;
    #endif
    #ifdef USE_PS_DATA
    #ifndef DESCRIPTOR_PS_DATA_CNT
    #define DESCRIPTOR_PS_DATA_CNT 4
    #endif
    /** Optional protocol specific data */
    Uint32    psData[DESCRIPTOR_PS_DATA_CNT];//4 for netcp & 2 for srio
    #endif
}* HostDesc, HostDescriptor;

/*********************************************************************/
/* Define the bit and word layouts for the Monolithic Pkt Descriptor.*/
/*********************************************************************/
typedef struct {
    #if (ENDIAN == _BE)
    /* word 0 */
    Uint32  type_id         : 2;  //always 0x2 (Monolithic Packet ID)
    Uint32  packet_type     : 5;
    Uint32  data_offset     : 9;
    Uint32  packet_length   : 16; //in bytes (65535 max)
    /* word 1 */
    Uint32  src_tag_hi      : 8;
    Uint32  src_tag_lo      : 8;
    Uint32  dest_tag_hi     : 8;
    Uint32  dest_tag_lo     : 8;
    /* word 2 */
    Uint32  epib            : 1;  //1=extended packet info block is present
    Uint32  reserved_w2     : 1;
    Uint32  psv_word_count  : 6;  //number of 32-bit PS data words
    Uint32  err_flags       : 4;
    Uint32  ps_flags        : 4;
    Uint32  reserved_w2b    : 1;
    Uint32  ret_push_policy : 1;  //0=return to queue tail, 1=queue head
    Uint32  pkt_return_qmgr : 2;
    Uint32  pkt_return_qnum : 12;
    #else
    /* word 0 */
    Uint32  packet_length   : 16; //in bytes (65535 max)
    Uint32  data_offset     : 9;
    Uint32  packet_type     : 5;
    Uint32  type_id         : 2;  //always 0x2 (Monolithic Packet ID)
    /* word 1 */
    Uint32  dest_tag_lo     : 8;
    Uint32  dest_tag_hi     : 8;
    Uint32  src_tag_lo      : 8;
    Uint32  src_tag_hi      : 8;
    /* word 2 */
    Uint32  pkt_return_qnum : 12;
    Uint32  pkt_return_qmgr : 2;
    Uint32  ret_push_policy : 1;  //0=return to queue tail, 1=queue head
    Uint32  reserved_w2b    : 1;
    Uint32  ps_flags        : 4;
    Uint32  err_flags       : 4;
    Uint32  psv_word_count  : 6;  //number of 32-bit PS data words
    Uint32  reserved_w2     : 1;
    Uint32  epib            : 1;  //1=extended packet info block is present
#endif
}* MonoDesc, MonolithicDescriptor;



#endif /* INCLUDE_HW_DESCRIPTOR_H_ */
