/*
 * Cpdma.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_CPDMA_H_
#define INCLUDE_HW_CPDMA_H_

#include <Type.h>

typedef enum {
    CpDma_QMSS = 0,
    CpDma_SRIO,
    CpDma_NETCP,
    CpDma_PASS = CpDma_NETCP,
    CpDma_LAST = CpDma_PASS,
    CpDma_CNT
} CpDma;

#define Cpdma_QMSS_MAX_CH_NUM          32
#define Cpdma_QMSS_MAX_RX_FLOW_NUM     64
#define Cpdma_SRIO_MAX_CH_NUM          16
#define Cpdma_SRIO_MAX_RX_FLOW_NUM     20
#define Cpdma_PASS_MAX_RX_CH_NUM       24
#define Cpdma_PASS_MAX_TX_CH_NUM       9
#define Cpdma_PASS_MAX_RX_FLOW_NUM     32

typedef struct {
    /** This field indicates the default receive queue that this channel should use */
    Uint16 dest_qnum;
    /** This field specifies the number of bytes that are to be skipped in the SOP buffer before beginning
     * to write the payload or protocol specific bytes(if they are in the sop buffer).  This value must
     * be less than the minimum size of a buffer in the system */
    Uint16 sop_offset;
    /** This field controls where the Protocol Specific words will be placed in the Host Mode CPPI data structure
     * 0 - protocol specific information is located in descriptor
     * 1 - protocol specific information is located in SOP buffer */
    Uint16 ps_location;
    /** This field indicates the descriptor type to use 1 = Host, 2 = Monolithic */
    Uint8 desc_type;
    /** This field controls the error handling mode for the flow and is only used when channel errors occurs
     * 0 = Starvation errors result in dropping packet and reclaiming any used descriptor or buffer resources
     * back to the original queues/pools they were allocated to
     * 1 = Starvation errors result in subsequent re-try of the descriptor allocation operation. */
    Uint16 error_handling;
    /** This field controls whether or not the Protocol Specific words will be present in the Rx Packet Descriptor
     * 0 - The port DMA will set the PS word count to 0 in the PD and will drop any PS words that are presented
     * from the back end application.
     * 1 - The port DMA will set the PS word count to the value given by the back end application and will copy
     * the PS words from the back end application to the location */
    Uint16 psinfo_present;
    /** This field controls whether or not the Extended Packet Info Block will be present in the Rx Packet Descriptor.
     * 0 - The port DMA will clear the Extended Packet Info Block Present bit in the PD and will drop any extended
     * packet info words that are presented from the back end application.
     * 1 - The port DMA will set the Extended Packet Info Block Present bit in the PD and will copy any extended packet
     * info words that are presented across the Rx streaming interface into the extended packet info words in the descriptor.
     * If no extended packet info words are presented from the back end application, the port DMA will overwrite the fields with zeroes. */
    Uint16 einfo_present;

    /** This is the value to insert into bits 7:0 of the destination tag if the rx_dest_tag_lo_sel is set to 1 */
    Uint8 dest_tag_lo;
    /** This is the value to insert into bits 15:8 of the destination tag if the rx_dest_tag_hi_sel is set to 1 */
    Uint8 dest_tag_hi;
    /** This is the value to insert into bits 7:0 of the source tag if the rx_src_tag_lo_sel is set to 1 */
    Uint8 src_tag_lo;
    /** This is the value to insert into bits 15:8 of the source tag if the rx_src_tag_hi_sel is set to 1 */
    Uint8 src_tag_hi;

    /** This field specifies the source for bits 7:0 of the source tag field in word 1 of the output PD.
     * This field is encoded as follows:
     * 0 = do not overwrite
     * 1 = overwrite with value given in rx_dest_tag_lo
     * 2 = overwrite with flow_id[7:0] from back end application
     * 3 = RESERVED
     * 4 = overwrite with dest_tag[7:0] from back end application
     * 5 = overwrite with dest_tag[15:8] from back end application
     * 6-7 = RESERVED */
    Uint8 dest_tag_lo_sel;
    /** This field specifies the source for bits 15:8 of the source tag field in the word 1 of the output PD.
     * This field is encoded as follows:
     * 0 = do not overwrite
     * 1 = overwrite with value given in rx_dest_tag_hi
     * 2 = overwrite with flow_id[7:0] from back end application
     * 3 = RESERVED
     * 4 = overwrite with dest_tag[7:0] from back end application
     * 5 = overwrite with dest_tag[15:8] from back end application
     * 6-7 = RESERVED */
    Uint8 dest_tag_hi_sel;
    /** This field specifies the source for bits 7:0 of the source tag field in the output packet descriptor.
     * This field is encoded as follows:
     * 0 = do not overwrite
     * 1 = overwrite with value given in rx_src_tag_lo
     * 2 = overwrite with flow_id[7:0] from back end application
     * 3 = RESERVED
     * 4 = overwrite with src_tag[7:0] from back end application
     * 5 = RESERVED
     * 6-7 = RESERVED */
    Uint8 src_tag_lo_sel;
    /** This field specifies the source for bits 15:8 of the source tag field in the output packet descriptor.
     * This field is encoded as follows:
     * 0 = do not overwrite
     * 1 = overwrite with value given in rx_src_tag_hi
     * 2 = overwrite with flow_id[7:0] from back end application
     * 3 = RESERVED
     * 4 = overwrite with src_tag[7:0] from back end application
     * 5 = RESERVED
     * 6-7 = RESERVED */
    Uint8 src_tag_hi_sel;

    /** This bits control whether or not the flow will compare the packet size received from the back end application
     * against the rx_size_thresh0 fields to determine which FDQ to allocate the SOP buffer from.
     * The bits in this field is encoded as follows:
     * 0 = Do not use the threshold.
     * 1 = Use the thresholds to select SOP FDQ rx_fdq0_sz0_qnum. */
    Uint8 size_thresh0_en;
    /** This bits control whether or not the flow will compare the packet size received from the back end application
     * against the rx_size_thresh1 fields to determine which FDQ to allocate the SOP buffer from.
     * The bits in this field is encoded as follows:
     * 0 = Do not use the threshold.
     * 1 = Use the thresholds to select SOP FDQ rx_fdq0_sz1_qnum. */
    Uint8 size_thresh1_en;
    /** This bits control whether or not the flow will compare the packet size received from the back end application
     * against the rx_size_thresh2 fields to determine which FDQ to allocate the SOP buffer from.
     * The bits in this field is encoded as follows:
     * 0 = Do not use the threshold.
     * 1 = Use the thresholds to select SOP FDQ rx_fdq0_sz2_qnum. */
    Uint8 size_thresh2_en;
    /** This value is left shifted by 5 bits and compared against the packet size to determine which free descriptor
     * queue should be used for the SOP buffer in the packet.  If the packet size is less than or equal to the value
     * given in this threshold, the DMA controller in the port will allocate the SOP buffer from the queue given by
     * the rx_fdq0_sz0_qnum fields. This field is optional. */
    Uint32 size_thresh0;
    /** This value is left shifted by 5 bits and compared against the packet size to determine which free descriptor
     * queue should be used for the SOP buffer in the packet.  If the  packet size is greater than the rx_size_thresh0
     * but is less than or equal to the value given in this threshold, the DMA controller in the port will allocate the
     * SOP buffer from the queue given by the rx_fdq0_sz1_qnum fields. */
    Uint32 size_thresh1;
    /** This value is left shifted by 5 bits and compared against the packet size to determine which free descriptor
     * queue should be used for the SOP buffer in the packet.  If the  packet size is less than or equal to the value
     * given in this threshold, the DMA controller in the port will allocate the SOP buffer from the queue given by the
     * rx_fdq0_sz2_qnum fields.
     * If enabled, this value must be greater than the value given in the rx_size_thresh1 field. This field is optional. */
    Uint32 size_thresh2;
    /** This field specifies which Free Descriptor Queue should be used for the 1st Rx buffer in a packet whose
     * size is less than or equal to the rx_size0 value */
    Uint16 fdq0_sz0_qnum;
    /** This field specifies which Queue should be used for the 1st Rx buffer in a packet whose size is
     * less than or equal to the rx_size1 value */
    Uint16 fdq0_sz1_qnum;
    /** This field specifies which Free Descriptor Queue should be used for the 1st Rx buffer in a packet
     * whose size is less than or equal to the rx_size2 value */
    Uint16 fdq0_sz2_qnum;
    /** This field specifies which Free Descriptor Queue should be used for the 1st Rx buffer in a
     * packet whose size is less than or equal to the rx_size3 value */
    Uint16 fdq0_sz3_qnum;
    /** This field specifies which Free Descriptor Queue should be used for the 2nd Rx buffer in a host type packet */
    Uint16 fdq1_qnum;
    /** This field specifies which Free Descriptor Queue should be used for the 3rd Rx buffer in a host type packet */
    Uint16 fdq2_qnum;
    /** This field specifies which Free Descriptor Queue should be used for the 4th or later Rx
     *  buffers in a host type packet */
    Uint16 fdq3_qnum;
}* RxFlowCfg, Cpdma_RxFlowConfiguration;


extern void __init
Cpdma_rxChannelEnable(
    CpDma   dma,
    Uint16  channel
);

extern void __init
Cpdma_txChannelEnable(
    CpDma   dma,
    Uint16  channel
);

extern void __init
Cpdma_globalEnable(
    CpDma   dma,
    Uint16  rxPriority,
    Uint16  txPriority,
    bool    loopback
);

extern void __init
Cpdma_rxFlowConfig(
    CpDma       dma,
    RxFlowCfg   cfg,
    Uint16      idx
);

#endif /* INCLUDE_HW_CPDMA_H_ */
