/*
 * Psc.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_Psc_H_
#define INCLUDE_HW_Psc_H_

#include <Type.h>
#include <ti/csl/cslr_device.h>

typedef enum {
    /** Module is in Reset state. Clock is off. */
    Psc_MODSTATE_SWRSTDISABLE = 0,
    /** Module is in Sync Reset state. */
    Psc_MODSTATE_SYNCRST = 1,
    /** Module is in disable state. */
    Psc_MODSTATE_DISABLE = 2,
    /** Module is in enable state. */
    Psc_MODSTATE_ENABLE = 3,
    /** Module is in Auto sleep state */
    Psc_MODSTATE_AUTOSLP = 4,
    /** Module is in Auto wake state */
    Psc_MODSTATE_AUTOWK = 5,

    /** Power domain is Off */
    Psc_PDSTATE_OFF = 0,
    /** Power domain is On */
    Psc_PDSTATE_ON = 1,

    Psc_SOK = 0,
    Psc_FAILED
} Psc_Status;

extern
Psc_Status __init    //@return : @see Psc_MODSTATE_XXX
Psc_moduleDisable(
    Uint16 pwrDmnNum,
    Uint16 moduleNum
);

extern
Psc_Status __init
Psc_moduleEnable(
    Uint16 pwrDmnNum,
    Uint16 moduleNum
);

extern
Psc_Status __init       //@retutn : @see Psc_PDSTATE_ON/OFF
Psc_powerDomainDisable(
    Uint16 pwrDmnNum
);


#endif /* INCLUDE_HW_Psc_H_ */
