/*
 * Uart.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_UART_H_
#define INCLUDE_UART_H_

#include <Type.h>

#define BAUD_PS     115200


extern
void __init
Uart_init(
    void
);

#define Uart_NO_RX_DATA -1
extern
Int16 __aid(Uart)      //valid if < uint8_MAX, Uart_NO_RX_DATA for no input available
Uart_getChar(
    void
);

extern
void __aid(Uart)
Uart_putChar(
    const char ch
);

extern
int __aid(Uart)
Uart_printf(
    const char* fmt,
    ...
);

#endif /* INCLUDE_UART_H_ */
