/*
 * Cpdma.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_PDSP_H_
#define INCLUDE_HW_PDSP_H_

#include <Type.h>
#include <Hw/Qmss.h>


#define Pdsp_ACC_48_CHANNEL 0//1

typedef enum {
    Pdsp_QMSS = 0,
    Qmss_PDSP1 = 0, Qmss_PDSP2, Qmss_PDSPS, Qmss_PDSP_INVALID,

    Pdsp_PASS = 1,
    Pass_PDSP0 = 0, Pass_PDSP1, Pass_PDSP2, Pass_PDSP3, Pass_PDSP4,
    Pass_PDSPS, Pass_PDSP_INVALID,
} Pdsp;


typedef enum {
    /** Accumulator command to disable channel */
    Pdsp_AccCmd_DISABLE_CHANNEL = 0x80,
    /** Accumulator command to enable channel */
    Pdsp_AccCmd_ENABLE_CHANNEL = 0x81,
    /** Accumulator command to configure timer constant */
    Pdsp_AccCmd_CONFIG_TIMER_CONSTANT = 0x82,
    /** Accumulator command to configure reclamation queue */
    Pdsp_AccCmd_CONFIG_RECLAIM_QUEUE = 0x83,
    /** Accumulator command to configure diversion queue */
    Pdsp_AccCmd_CONFIG_DIVERSION_QUEUE = 0x84
} Pdsp_AccCmdType;

typedef enum {
    /** Interrupt on entry threshold count only */
    Pdsp_AccPacingMode_NONE = 0,
    /** Time delay since last interrupt */
    Pdsp_AccPacingMode_LAST_INTERRUPT,
    /** Time delay since first new packet */
    Pdsp_AccPacingMode_FIRST_NEW_PACKET,
    /** Time delay since last new packet */
    Pdsp_AccPacingMode_LAST_NEW_PACKET
} Pdsp_AccPacingMode;

typedef enum {
    /** 'D' register only (4 byte entries)
     * Word 0 : Packet Descriptor Pointer */
    Pdsp_AccEntrySize_REG_D = 0,
    /** 'C,D' registers (8 byte entries)
     * Word 0 : Packet Length (as reported by queue manager)
     * Word 1 : Packet Descriptor Pointer */
    Pdsp_AccEntrySize_REG_CD,
    /** 'A,B,C,D' registers (16 byte entries)
     * Word 0 : Packet Count on Queue (when read)
     * Word 1 : Byte Count on Queue (when read)
     * Word 2 : Packet Length (as reported by queue manager)
     * Word 3 : Packet Descriptor Pointer */
    Pdsp_AccEntrySize_REG_ABCD
} Pdsp_AccEntrySize;

typedef enum {
    /** NULL Terminate Mode - The last list entry is used to store a NULL pointer
     * record (NULL terminator) to mark the end of list. In either case there is room for one less
     * list entry in a page than is actually specified by the host. */
    Pdsp_AccCountMode_NULL_TERMINATE = 0,
    /** Entry Count Mode - The first list entry is used to store the total list entry
     * count (not including the length entry). */
    Pdsp_AccCountMode_ENTRY_COUNT
}Pdsp_AccCountMode;

typedef enum {
    /** Single Queue Mode - The channel monitors a single queue. */
    Pdsp_AccQueueMode_SINGLE_QUEUE = 0,
    /** Multi-Queue Mode - The channel monitors up to 32 queues starting at the supplied base queue index. */
    Pdsp_AccQueueMode_MULTI_QUEUE
} Pdsp_AccQueueMode;

typedef struct {
    /** Accumulator channel affected (0-47) */
    Uint8               channel;
    /** Accumulator channel command - Pdsp_AccCmd_ENABLE_CHANNEL : Enable channel
     * Pdsp_AccCmd_DISABLE_CHANNEL : Disable channel */
    Pdsp_AccCmdType     command;
    /** This field specifies which queues are to be included in the queue group.
     * Bit 0 corresponds to the base queue index, and bit 31 corresponds to the base
     * queue index plus 31. For any bit set in this mask, the corresponding queue index
     * is included in the monitoring function.
     * *This field is ignored in single-queue mode.*/
    Uint32              queueEnMask;
    /** Physical pointer to list ping/pong buffer. NULL when channel disabled */
    Uint32              listAddress;
    /** Queue Manager and Queue Number index to monitor. This serves as a base queue index when the
     * channel in multi-queue mode, and must be a multiple of 32 when multi-queue mode is enabled. */
    Uint16              queMgrIndex;
    /** Max entries per list buffer page */
    Uint16              maxPageEntries;
    /** Number of timer ticks to delay interrupt */
    Uint16              timerLoadCount;
    /** Interrupt pacing mode. Specifies when the interrupt should be trigerred */
    Pdsp_AccPacingMode  interruptPacingMode;
    /** List entry size. Specifies the size of each data entry */
    Pdsp_AccEntrySize   listEntrySize;
    /** List count Mode. The number of entries in the list */
    Pdsp_AccCountMode   listCountMode;
    /** Queue mode. Moitor single or multiple queues */
    Pdsp_AccQueueMode   multiQueueMode;
}* PdspAccCfg, Pdsp_AccCmdCfg;

extern
Uint16 __init
Pdsp_accumulatorSetup(
    Pdsp,
    PdspAccCfg
);

extern
void __init
Pdsp_firmwareLoad(
    Pdsp pid,
    const void* fw,
    Uint32 count
);

extern
Uint32 __init
Pdsp_reclaimQueueSetup(
    Pdsp  pid,
    Queue que
);

#endif /* INCLUDE_HW_PDSP_H_ */
