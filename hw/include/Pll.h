/*
 * Pll.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_PLL_H_
#define INCLUDE_HW_PLL_H_

#include <Type.h>


extern void __init Pll_init(void);

#endif /* INCLUDE_HW_PLL_H_ */
