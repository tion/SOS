/*
 * Qmssd.c
 *
 *  Created on: Oct 14, 2018
 *      Author: ax
 */
#define MODULE "[QM]: "

#include <Console.h>
#include <Hw/Qmss.h>

static __data(daemon:Qmss) Queue q;

int
__daemon(Qmss)
qm(
    int argc,
    void* argv[]
){

    char cmd = *((char*) argv[1]);

    q = strtoul((char*) argv[2], NULL, 0);
    if (q > Queue_MAX) {
        Console_printf(MODULE"invalid queue - %d.\n", q);
        return -1;
    }

    Descriptor desc = 0;
    if (argc > 3) {
        desc = strtoul((char*) argv[3], NULL, 0);
        if (desc == 0) {
            Console_printf(MODULE"invalid descriptor - %x.\n", desc);
            return -2;
    }   }

    short size = 0;
    if (argc > 4) {
        size = atoi((char*) argv[4]);
        if (size >= Desc_HOST_FETCH_SIZE_CNT) {
            Console_printf(MODULE"invalid size - %x.\n", size);
            return -3;
    }   }


    switch (cmd) {
    case 'e':
        Qmss_push(q, desc | Desc_fsize(size));
        break;

    case 'd':
        desc = Qmss_popRaw(q);
        Console_printf(MODULE"Queue %d poped 0x%x.\n",q, desc);
        break;

    case 'c':   //count before return;
    default:    //get count in queue
        do {;} while (0);
    }
    Console_printf(MODULE"%d descriptor(s) in Queue %d.\n",Qmss_getEntryCnt(q) ,q);


    return 0;
}
