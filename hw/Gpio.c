/*
 * Gpio.c
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */
#include <Hw/Gpio.h>

#include <ti/csl/csl_gpio.h>

CSL_GpioHandle const hGpio = (CSL_GpioHandle) CSL_GPIO_REGS;

void
Gpio_set(
    Bits16 pins
){
    hGpio->BANK_REGISTERS[0].SET_DATA = pins;
}

void
Gpio_clear(
    Bits16 pins
){
    hGpio->BANK_REGISTERS[0].CLR_DATA = pins;
}

Bits16
Gpio_getInStatus(
    Bits16 pins
){
    return hGpio->BANK_REGISTERS[0].IN_DATA;
}

Bits16
Gpio_getOutStatus(
    Bits16 pins
){
    return hGpio->BANK_REGISTERS[0].OUT_DATA;
}

void
Gpio_setDirIn(
    Bits16 pins
){
    hGpio->BANK_REGISTERS[0].DIR &= ~pins;
}

void
Gpio_setDirOut(
    Bits16 pins
){
    hGpio->BANK_REGISTERS[0].DIR |= pins;
}

void
Gpio_pinSetup(
    Bits16 pins,
    Bits16 attr
){
    if (attr & GP_DIR_OUT) {
        hGpio->BANK_REGISTERS[0].DIR |= pins; }
    else {
        hGpio->BANK_REGISTERS[0].DIR &= ~pins;
        if (attr & GP_SET)
            hGpio->BANK_REGISTERS[0].SET_DATA = pins;
        else
            hGpio->BANK_REGISTERS[0].CLR_DATA = pins;
    }

    if (attr & GP_SET_RAISE_EDGE_INT)
        hGpio->BANK_REGISTERS[0].SET_RIS_TRIG = pins;
    else
        hGpio->BANK_REGISTERS[0].CLR_RIS_TRIG = pins;

    if (attr & GP_SET_FALL_EDGE_INT)
        hGpio->BANK_REGISTERS[0].SET_FAL_TRIG = pins;
    else
        hGpio->BANK_REGISTERS[0].CLR_FAL_TRIG = pins;

    hGpio->BINTEN = 1;
}
