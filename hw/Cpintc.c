/*
 * Cpintc.c
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */
#include <Hwi.h>

#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_cpIntcAux.h>


static CSL_CPINTC_RegsOvly const hCpintc[] = {
      (CSL_CPINTC_RegsOvly) CSL_CP_INTC_0_REGS,
      (CSL_CPINTC_RegsOvly) CSL_CP_INTC_1_REGS,
      (CSL_CPINTC_RegsOvly) CSL_CP_INTC_2_REGS,
      (CSL_CPINTC_RegsOvly) CSL_CP_INTC_3_REGS };


void
Cpintc_route(
    Cpintc cic,
    Uint16 sysInt,
    Uint16 hostInt
){
    CSL_CPINTC_Handle hCic = (CSL_CPINTC_Handle) hCpintc[cic];
    CSL_CPINTC_disableAllHostInterrupt(hCic);
    CSL_CPINTC_mapSystemIntrToChannel(hCic, sysInt, hostInt);
    CSL_CPINTC_clearSysInterrupt(hCic, sysInt);
    CSL_CPINTC_enableSysInterrupt(hCic, sysInt);
    CSL_CPINTC_enableHostInterrupt(hCic, hostInt);
    CSL_CPINTC_enableAllHostInterrupt(hCic);
}


void
Cpintc_clearSysInt(
    Cpintc cic,
    Uint16 sysInt
){
    CSL_CPINTC_Handle hCic = (CSL_CPINTC_Handle) hCpintc[cic];
    CSL_CPINTC_clearSysInterrupt(hCic, sysInt);
}
