/*
 * Device.c
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */
#include <c6x.h>

#include <Timer.h>
#include <Device.h>
#include <Hw/Xmc.h>
#include <Hw/Pll.h>
#include <Hw/Emif.h>
#include <Hw/Uart.h>

#include <ti/csl/csl_bootcfgAux.h>


void
Hw_socSetup(
    CorePac core
){
    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    #if !defined(BUILD_BOOTLOADER) && INIT_TSC
    TSCL = 0;
    #endif

    if (core == COREPAC_MASTER) {// soc level configuration.
        Emif16_init();
        #if INIT_PLL
        Pll_init();
        #endif
        #if INIT_DDR
        Emif4f_init();
        #endif
        #if INIT_UART
        Uart_init();
        #endif
    }

    /* core pack level configuration - memory map setup now.
    Xmc_setup(core, XMC_MAP); TODO XXX XMC_map */

    CSL_BootCfgLockKicker();
}
