/*
 * System.c
 *
 *  Created on: Oct 29, 2018
 *      Author: ax
 */
#include <stdio.h>
#include <string.h>

#include <Log.h>
#include <Device.h>
#include <System.h>
#include <Process.h>

#define __HYPLNK_INIT_HERE__
#include <Hw/Qmss.h>
#include <Hw/Netcp.h>
#include <Socket/Ipnet.h>
#include <Socket/Ipnet/Udp.h>
#include <Socket/Link/Ethernet.h>

#include <Sos.h>


void __weak
System_preSetup(
        void
){
    #ifdef _DBG
    Hw_socSetup((CorePac) 0);
    Log_dbg("[Hw]: SOC setup!\n:");
    #endif

    //Hyplnk_init((void*)Hyplnk_MapTable);
    //Srio_init(Soc_getId() * 0x10, 4, 4, Srio_GarbageQueue, DIGITAL_LOOPBACK);
    Netcp_init(Netcp_PORT_ALL, Eth_MTU, Loopback_NO);

    Qmss_setup();
    //Srio_setup();
    Netcp_setup();

    return;
}


void __weak
System_postSetup(
        void
){
    return;
}


PROC_SIMPLE(udpd);
PROC_SIMPLE(Ttyd);
PROC_SIMPLE(Consoled);


void __weak
System_setup(
    void
){
    Process_getIdByName(udpD)
        = Process_create((Process) udpd, 0, 0, NULL, Process_READY);

    Process_getIdByName(ttyD)
        = Process_create((Process) Ttyd, 0, 0, NULL, Process_READY);

    Process_getIdByName(cosD)
        = Process_create((Process) Consoled, 0, 0, NULL, Process_READY);
}


void __weak
main(void) {
    System_preSetup();

    System_setup();

    System_postSetup();

    System_start();
    //shall not return
}
