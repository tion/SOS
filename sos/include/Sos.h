//
// Created by ax on 9/21/18.
//

#ifndef _SOS_CFG_H
#define _SOS_CFG_H


#define ARCH
#define CORE
#define PLATFORM
#define SOC
#define BOARD
#define MACHINE
#define SYS_ALAIS
#define SYS_CODE

#define Sos_Hook_

//TODO add more

////////////////* Process *////////////////
#define Process_TINY_SUPPORT   8
#define Process_SMALL_SUPPORT  32
#define Process_MIDDLE_SUPPORT 128
#define Process_LARGE_SUPPORT  512
#define Process_HUGE_SUPPORT   2048
#define Process_MAXMUM_SUPPORT 4096 - 1
//configuration for process max support
#define Process_SUPPORT_CNT Process_TINY_SUPPORT
//Definition
#ifdef _SOS_VAR_DEFINITION_
#ifndef Process_SUPPORT_CNT
#define Process_SUPPORT_CNT Process_MIDDLE_SUPPORT
#endif
System_Info __knl(data) Sos = { 0 };
const Sid Process_SupportCount = Process_SUPPORT_CNT;
#endif

/* progress list blew */
#ifndef _LNK
PROC_ID(udpD);  //UDP echo test
PROC_ID(ttyD);  //UART tty daemon
PROC_ID(cosD);  //console daemon
#endif


////////////////* Queue *////////////////
#ifdef Queue_entity
Queue_entity(qtest1, 16);
Queue_entity(qtest2, 8);
#endif


////////////////* Semaphore *////////////////
#ifdef Semaphore_entity
Semaphore_entity(test0, 4);
Semaphore_entity(test1, 8);
#endif


#endif // _SOS_H
