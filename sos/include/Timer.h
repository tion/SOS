/*
 * Timer.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_TIMER_H_
#define INCLUDE_HW_TIMER_H_

#include <c6x.h>
#include <Type.h>

static inline
Timestamp
Timer_getStamp(
    void
){
    return _itoll(TSCH, TSCL);
}

#endif /* INCLUDE_HW_TIMER_H_ */
