/*
 * Ip.h
 *
 *  Created on: Oct 14, 2018
 *      Author: ax
 */

#ifndef INCLUDE_SOCKET_IPNET_H_
#define INCLUDE_SOCKET_IPNET_H_

#include <Type.h>

#define Ip_MTU      1500

typedef Uint32   Ipn4;

typedef struct __packed {
    Uint8   VerLen;
    Uint8   Tos;
    Uint16  TotalLen;
    Uint16  Id;
    Uint16  FlagOff;
    Uint8   Ttl;
    Uint8   Protocol;
    Uint16  Checksum;
    Ipn4    IPSrc;
    Ipn4    IPDst;
    Uint8   Options[0];
} Ip4Head;
#define IP_HDR_SIZE  (sizeof(Ip4Head))

typedef union __packed {
    Uint8   by[16];
    Uint16  db[8];
    Uint32  wd[4];
    Uint64  ll[2];
} Ipn6;

typedef struct __packed {
    Uint32   Ver: 4;
    Uint32   Tos: 8;
    Uint32   Flg: 20;

    Uint16  Len;
    Uint8   Ext;
    Uint8   Hop;

    Ipn6    SrcIp;
    Ipn6    DstIp;
} Ip6Head;

typedef struct __packed {
    Uint16  SrcPort;
    Uint16  DstPort;
    Uint16  Length;
    Uint16  Checksum;
} UdpHead;

typedef struct __packed {
    Uint16  REQ;
    Uint16  ACK;
    Uint16  LEN;
    Uint16  CKSM;
    Uint8   DATA[];
}* RawIPkt, RawIpPacket;

typedef union __packed {
    struct __packed {
        UdpHead Head;
        Uint8   Payload[];  } Udp;
        RawIpPacket           Raw;
} IpDatgrm;


typedef struct __packed {

} NDPPacket;

typedef struct __packed {
        Ip4Head  Head;
        IpDatgrm Datgrm;    }* Ip4Pkt, Ipv4Packet;

typedef struct __packed {
        Ip6Head  Head;
        IpDatgrm Datgrm;    }* Ip6Pkt, Ipv6Packet;


#endif /* INCLUDE_SOCKET_IPNET_H_ */
