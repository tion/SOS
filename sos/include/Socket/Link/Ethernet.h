/*
 * Ethernet.h
 *
 *  Created on: Oct 14, 2018
 *      Author: ax
 */

#ifndef INCLUDE_SOCKET_ETHERNET_H_
#define INCLUDE_SOCKET_ETHERNET_H_


#define Eth_ARP_NUM             4           //FIXME ARP entry support

#define Eth_HDR_SIZE            (6+6+2)     //2 x MAC[6] + U16 TYPE
#define Eth_MTU                 (Ip_MTU + Eth_HDR_SIZE + 4)   //MTU + Head + CRC
#ifdef USE_ETH_OOB
#define Eth_OOB                 16          //sizeof(Preamble), also an alignment xxx
#else
#define Eth_OOB                 0
#endif
#define Eth_MAC_SIZE            6
#define Eth_SIZE_MIN            (64 - 4)    //CRC AUTO

#define Eth_BUFFER_ALIGN        0x0010      //OOB
#define Eth_FRAME_ALIGN_MSK     (64 -1)
#define EthFrame_align(buffer)  ((EthFrame) ((Uint32) buffer & ~Eth_FRAME_ALIGN_MSK))


typedef union __packed {
    Uint8             Hex[Eth_MAC_SIZE];
    struct __packed {
        Uint16 H;
        Uint32 L;   } Val;              } EthMac;

typedef struct __packed {
    Uint16  HardType;
    Uint16  ProtocolType;
    Uint8   HardSize;
    Uint8   ProtocolSize;
    Uint16  Op;
    EthMac  SMac;
    Ipn4    SIp4;
    EthMac  DMac;
    Ipn4    DIp4;
}* Arp, ArpPacket;

typedef struct __packed {
    #ifdef USE_ETH_OOB
    union __packed {
        Uint8               Preamble[8];
        struct __packed {
            PtrVal Descriptor;
            Ptr    Magic;
            Uint8  Pad[8];  } Ext;  } Oob;
    #endif
    struct __packed {
        EthMac      Dst;
        EthMac      Src;
        Uint16      Type;           } Head;
    union __packed {
        Ipv4Packet  Ip4;
        Ipv6Packet  Ip6;
        NDPPacket   Ddp;
        ArpPacket   Arp;            } Packet;
    #if Ethernet_PADDING
        Uint8                         Pad[Ethernet_PADDING_DAT]
    #endif
    #if Ethernet_CRC_MANUAL
    Uint32                            Crc;
    #endif
}* EthFrame, Ethernet_Frame;


typedef struct {
    //Ipn6     ip;
    Ipn4    Ip;
    EthMac  Mac;
} ArpEntry;
extern ArpEntry Eth_ARP_TABLE[Eth_ARP_NUM];

#endif /* INCLUDE_SOCKET_ETHERNET_H_ */
