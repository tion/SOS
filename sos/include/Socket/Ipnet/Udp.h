/*
 * Udp.h
 *
 *  Created on: Oct 14, 2018
 *      Author: ax
 */

#ifndef INCLUDE_SOCKET_UDP_H_
#define INCLUDE_SOCKET_UDP_H_

#include <Type.h>


typedef enum {
    Udp_SOK = 0,
    Udp_INVALID_MSG_BUFF,
    Udp_OPEN_ARP_TIMEOUT,
    Udp_PACKET_MTU_OV,
    Udp_TX_BD_LOST,
    Udp_INALID = -1
} Udp_Status;


typedef Uint32 Ipn;

static inline
Ipn
Ip_makeIp4(
        Uint8 msbA,
        Uint8 msbB,
        Uint8 msbC,
        Uint8 lsbD
){
    return Type_QByteVal(msbA, msbB, msbC, lsbD);
}


#define UDPHDR_SIZE     8
typedef struct __packed {
    Uint16  SrcPort;
    Uint16  DstPort;
    Uint16  Length;
    Uint16  UDPChecksum;
    Uint8   Payload[0];
}* Udp_Msg, UDPHDR;

#define Udp_OpenTimeout 25000000//25ms
#define Udp_OpenRetry   5

extern
Udp_Status __init
Udp_open(
    Ipn iplocal,
    Ipn peer
);

Udp_Msg     //can always succed in returing an message buffer to send
Udp_alloc(  //alloc a udp buffer to send
    void
);

Udp_Status
Udp_send(
    Udp_Msg //MUST be alloc by Udp_alloc, unknown behavior for invalid msg.
);

Udp_Msg
Udp_receive(
    void
);

void
Udp_free(
    Udp_Msg
);


#endif /* INCLUDE_SOCKET_UDP_H_ */
