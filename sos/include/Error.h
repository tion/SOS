/*
 * Error.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_ERROR_H_
#define INCLUDE_ERROR_H_

#include <Type.h>


typedef Int16 ReturnCode;
typedef ReturnCode RC;
typedef Int16 Error_Number;

#define Error_Code          Return
#define ERROR_MSK           0x8000
#define ERROR_SHIFT         15
#define ERROR_MODULE_MSK    0x3F00
#define ERROR_MODULE_SHIFT  8
#define ERROR_NUM_MSK       0x00FF

#define RETURN_CODE(E, M, S) ( (Error_Number) ( \
                             (((E) << 15) & ERROR_MSK) \
                             (((M) <<  8) & ERROR_MODULE_MSK) \
                             (((S) <<  0) & ERROR_NUM_MSK) ) )
#define ERROR(M, E)         RETURN_CODE(YES, M, E)
#define STATUS(M, S)        RETURN_CODE(NO, M, S)


#define SOK                 0
#define FAULT               -1


extern Error_Number* Progress_getError(Process_Id);

#define errno *(Progress_getError(Progress_SELF))

Error_Number
Progress_getErrorNumber(
    Process_Id prog
);


#endif /* INCLUDE_ERROR_H_ */
