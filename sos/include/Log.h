/*
 * Log.h
 *
 *  Created on: Oct 14, 2018
 *      Author: ax
 */

#ifndef INCLUDE_LOG_H_
#define INCLUDE_LOG_H_


#define LOG_OUT_NULL        0
#define LOG_OUT_CCS_CONSOLE 1
#define LOG_OUT_UART        2


#define LOG_OUT             2

#define Log_null(x, ...)    do {/*nothing*/} while (0)
#include <Type.h>
#if LOG_OUT == LOG_OUT_CCS_CONSOLE
#include <stdio.h>
#define PRINTF printf
#elif LOG_OUT == LOG_OUT_UART
#include <Hw/Uart.h>
#define PRINTF Uart_printf
#else
#define PRINTF Log_null
#endif

//TODO add an trace
#define Trace   PRINTF

#ifndef MODULE
#define MODULE "[XXX]: "
#endif


//////log level
#define Log_FAULT       0
#define Log_ERROR       1
#define Log_DEFECT      2
#define LOG_TRACE       3
#define Log_WARN        4
#define Log_MARK        5
#define Log_INFO        6
#define Log_DBG         7


#ifdef _DBG
#define LOG_OUT_LEVEL       Log_DBG
#else
#define LOG_OUT_LEVEL       Log_WARN//TODO FIXME proper level
#endif

#if Log_DBG <= LOG_OUT_LEVEL
#define Log_dbg PRINTF
#else
#define Log_dbg Log_null
#endif

#if Log_INFO <= LOG_OUT_LEVEL
#define Log_info PRINTF
#else
#define Log_info Log_null
#endif

#if Log_MARK <= LOG_OUT_LEVEL
#define Log_mark Trace
#else
#define Log_mark Log_null
#endif

#if Log_WARN <= LOG_OUT_LEVEL
#define Log_warn PRINTF
#else
#define Log_warn Log_null
#endif

#if Log_TRCE <= LOG_OUT_LEVEL
#define Log_trace(str) Trace(MODULE #str"\n")
#else
#define Log_trace Log_null
#endif

#if Log_ERROR <= LOG_OUT_LEVEL
#define Log_error PRINTF
#else
#define Log_error Log_null
#endif

//defect & fault will always log out
#define Log_fault   PRINTF
#define Log_defect  Log_fault

//Alias, Log_null if you wish a cleaned diagnose environment.
#define log                 Log_info
#define Log_msg             Log_info
#define WTF                 Log_null
#define Uh                  WTF

#define TODO(str)           Log_mark(MODULE"--TODO-- "str" @ %s : %s L%d.\n", __FILE__, __FUNCTION__, __LINE__)

//TODO add Log_record(Level, log ...) to xxx

#endif /* INCLUDE_LOG_H_ */
