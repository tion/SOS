/*
 * Cpintc.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_HW_HWI_H_
#define INCLUDE_HW_HWI_H_

#include <Type.h>

typedef enum {
    CPINTC_0 = 0, CPINTC_1, CPINTC_2, CPINTC_3,
    CPINTC_MAX, CPINTC_INVALID
} Cpintc;

extern
void
Cpintc_route(
    Cpintc cic,
    Uint16 sysInt,
    Uint16 hostInt
);

extern
void
Cpintc_clearSysInt(
    Cpintc cic,
    Uint16 sysInt
);


/** Map CSL_INTC0_VUSR_INT_O @CIC0 to hostInt 33/event22 & Act@Hwi6 */
#define Hyplnk_CIC              CPINTC_0
#define Hyplnk_HOST_INT         33
#define Hyplnk_HOST_INT_EVENT   22
#define Hwi_Hyplnk              6



typedef Uint32 Hwi_Key;
/* interrupt service routine */
typedef void (*Isr)(void*);
/* mark a Isr */
#define isr interrupt


/* These intrinsic prototypes are missing from <c6x.h>.
 * Add them here to keep Coverity happy. */
extern Hwi_Key _disable_interrupts(void);
extern Hwi_Key _enable_interrupts(void);
extern void _restore_interrupts(Hwi_Key);


/* ======== Hwi_disable ======== */
#define Hwi_disable() _disable_interrupts()
/* ======== Hwi_enable ======== */
#define Hwi_enable() _enable_interrupts()
/* ======== Hwi_restore ======== */
#define Hwi_restore(key) _restore_interrupts(key)

#endif /* INCLUDE_HW_HWI_H_ */
