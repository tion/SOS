/*
 * Common.h
 *
 *  Created on: Oct 13, 2018
 *      Author: ax
 */

#ifndef INCLUDE_DEVICE_H_
#define INCLUDE_DEVICE_H_

#include <Type.h>


#define CLK_MAIN    1000000000  //1000M Hz


typedef enum {
    COREPAC_MASTER = 0,
    COREPAC0 = COREPAC_MASTER,
    COREPAC1,
    COREPAC2,
    COREPAC3,
    COREPAC4,
    COREPAC5,
    COREPAC6,
    COREPAC7,
    ALL_CORES,
    CORE_ENTRY_MAX
} CorePac;

typedef enum {
    Module_INTERNAL_LAST = 31,
    Module_QMSS,
    Module_CPDMA,
    Module_NETCP,
    Module_SRIO,
    Module_SIO,
    Module_GIO,
    Module_Max,
    Module_INVALID
} Module;


extern
void
nop(
    IN NanoSecond
);

typedef Uint16 SocId;
extern SocId Hw_geSoctId(void);

typedef Uint16 CoreId;
extern CoreId Hw_getCoreId(void);


/* ----setup configuration--- */
#define INIT_TSC    1       //start corepac's time stamp
#define INIT_PLL    1       //need? RBL may init this.
#define INIT_DDR    1       //configure DDR3.
#define INIT_UART   1
#define INIT_XMMAP  0       //xmc memory map
#define INIT_ROBUST 0       //robust feather such as EDC, memory protection etc.


extern void __init
Hw_socSetup(
    CorePac core
);

#endif /* INCLUDE_DEVICE_H_ */
