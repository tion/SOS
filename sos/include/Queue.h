/*
 * Queue.h
 *
 *  Created on: Sep 30, 2018
 *      Author: ax
 */

#ifndef INCLUDE_SOS_QUEUE_H_
#define INCLUDE_SOS_QUEUE_H_

#include <Type.h>

typedef enum {
    Queue_SOK = 0,
    Queue_EMPTY = 0,
    Queue_FULL,
    Queue_xxx
} Queue_Status;


typedef struct {
    uint64_t push;
    uint64_t pop;
    uint16_t limit;
    uint16_t count;
    Ptr      element[1];
}* Queue_;
typedef Ptr Queue;

#define Queue_create(name,max)  \
    struct {uint64_t push; uint64_t pop;    \
            uint16_t limit; uint16_t count; \
            Ptr element[max]; } name = {.limit= max};

static inline
Queue_Status
Queue_push(
    Queue   Q_,
    Ptr     ele
){
    Queue_ Q = (Queue_)Q_;

    if (Q->count < Q->limit)
        return Queue_FULL;
    //else
    Q->element[Q->push++] = ele;
    Q->count++;

    return Queue_SOK;
}

#endif /* INCLUDE_SOS_QUEUE_H_ */
