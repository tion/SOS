/*
 * Console.h
 *
 *  Created on: Oct 14, 2018
 *      Author: ax
 */

#ifndef INCLUDE_CONSOLE_H_
#define INCLUDE_CONSOLE_H_

#include <Type.h>


//default printf
extern Printf Console_printf;

#define Console_CMD_ARGS_CNT_MAX    16
#define Console_CMD_LINE_MAX_LEN    (0x100 - 8)

typedef Uint16      Cmd;
#define CMD(A, B)   ((A << 8) | B)
#define CMDSTUB(func, module)   extern int __daemon(module) func(int argc, void* argv[])

CMDSTUB(qm, Qmss);
CMDSTUB(mw, Mem);
CMDSTUB(mm, Mem);
CMDSTUB(md, Mem);

//invalid TODO insert new cmd above.
CMDSTUB(iv, Console);


extern
void __aid(tty)
Tty_commandLineFree(
    void* line
);

extern
char*
Tty_commandLineAlloc(
    Printf outf
);

#endif /* INCLUDE_CONSOLE_H_ */
