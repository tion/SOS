/*
 * system.h
 *
 *  Created on: Sep 22, 2018
 *      Author: ax
 */

#ifndef INCLUDE_SYSTEM_H_
#define INCLUDE_SYSTEM_H_

#include <Type.h>


typedef enum {
    Sos_OK = 0,
    SOS_STARTUP = 0,
    SOS_IDLE,
    SOS_HWI_ISR_ENTER,
    SOS_HWI_ISR_STAGE0,
    SOS_HWI_EVTX_STAGE1,
    SOS_HWI_EVTY_STAGE1,
    SOS_HWI_EVTZ_STAGE1,
    SOS_PROGRESS_SCHEDUAL,
    SOS_PROGRESS_BREAKABLE,
    SOS_PROGRESS_NON_BREAKABLE,
    SOS_PROGRESS_PEND_DISPATCH,
    SOS_PROGRESS_DELAY_DISPATCH,
    SOS_PROGRESS_MERGE_SCHEDUAL,
//    SOS_PROGRESS_KERNAL_DISPATCH,
    SOS_PROGRESS_PEND_TIMEOUT_DISPATCH,
    SOS_STATUS_MAX,
} System_Status;


static inline
Uint32
System_nameVal(
    Uint8 c0,
    Uint8 c1,
    Uint8 c2,
    Uint8 c3
){
#if (ENDIAN == _BE)
    return ((Uint32) c0 << 24) | ((Uint32) c1 << 16) | ((Uint32) c2 << 8) | c3;
#else
    return ((Uint32) c3 << 24) | ((Uint32) c2 << 16) | ((Uint32) c1 << 8) | c0;
#endif
}

typedef enum {
    Sos_Kernal = 0,
    Sos_Progress = Sos_Kernal,
    Sos_Timestamp,
    Sos_Semphore,
    Sos_Socket,
    Sos_Error,
    Sos_Debug,
    Sos_Mutex,
    Sos_Timer,
    Sos_Event,
    Sos_Log,

    MUSR_START,
    MTODO_add_user_defined_below = MUSR_START,

    Modules,
    ModuleMaxSupport = 64
} System_Module;

//generate system wide module error code for return, status 0 means OK
#define System_Code(            \
    /*System_Module*/ module,   \
    /*Sid*/           status    \
)                               \
    ((0 == status) ? status : ((module << 10) | status))


#define System_Id(mudule, count)    System_Code(mudule, count)
#define System_IdNone               0
#define System_IdInvalid            ~0


/* users may provide user defined pre-setup & post-setup implementation;
 * they are called in main, before system's setup for pre-setup;
 *  & between system's setup and progress' dispatch. */
extern
void
System_preSetup(
        void
);

extern
void
System_postSetup(
    void
);

extern
void
System_setup (
    void
);

/*  */
extern
void
System_start(   /* start progress dispatch */
    void
);

#endif /* INCLUDE_SYSTEM_H_ */
