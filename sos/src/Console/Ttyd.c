/*
 * Tty.c
 *
 *  Created on: Nov 5, 2018
 *      Author: ax
 *
 *  This is a UART tty example implementation.
 */
#include <Hw/Uart.h>

#include <Console.h>
#include <Process.h>

#include <Sos.h>


static Uint16 pos = 0;
static char* line = NULL;


static void TTYd(void) {
    Int16 ch;
    while ((ch = Uart_getChar()) != Uart_NO_RX_DATA) {
        *(line + pos++) = ch;
        Uart_putChar((char) ch);

        if (ch == '\r') {
            Uart_putChar('\n');
            *(line + pos++) = '\0';
            Tty_commandLineFree(line);

            line = Tty_commandLineAlloc(Uart_printf);
            pos = 0;
        }
    }
}

void Ttyd(void) {
    if (NULL == line) {
        line = Tty_commandLineAlloc(Uart_printf);
        Process_redirect(Process_SELF, (Process) TTYd, 0, NULL);
    }
}
