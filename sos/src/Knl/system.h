/*
 * system.h
 *
 *  Created on: Sep 26, 2018
 *      Author: ax
 */

#ifndef SOS_INC_SYSTEM_H_
#define SOS_INC_SYSTEM_H_

#include "process.h"

typedef struct {
    Uint32                          status;

    struct {
        Process_Handler    current;
        Process_Handler    next;
        Process_Handler    merge;

        #ifdef _DBG
        uint64_t        tick;
        uint64_t        idle;
        uint64_t        cut;
        #endif

        struct {
//        Process_Id first;
//        Process_Id lastTimeout;
//        Process_Id tail;
            Process_Cnt count;
        } pend;
        struct {
            Process_Cnt count;
        } suspend;
        Process_Cnt    inDelay/*Count*/;
        Process_Cnt    inScheduler/*Count*/;

        Process_Cnt    intotal;
        Uint16    status;

        Process_ControlBlock pcb[8];//vLen
    }                           process;//keep &process.pcb @tail.
} System_Info;

extern System_Info Sos;


static inline
System_Status
System_getStatus(
    System_Status next
){
#if TIME_STATISTICS
#endif
    return (System_Status) Sos.status;
}

static inline
bool
System_processHaveMark(
    Process_Status ps
){
    return Sos.process.status & ps;
}

static inline
void
System_processMark(
    Process_Status ps
){
    Sos.process.status |= ps;
}

static inline
void
System_processDemark(
    Process_Status ps
){
    Sos.process.status &= ~ps;
}
//
//static inline
//Process_Handler
//Process_currentProcess(
//    void
//){//TODO fixme after XXX
//    return NULL;
//}

//static inline
//Process_currentProcessId(
//    void
//){//TODO fixme after XXX
//    return 0;
//}

#ifndef System_StatusTraceEnable
static inline void System_mark(System_Status ss){}
static inline void System_marked(System_Status ss){}
#else

static inline
void
System_mark(
    System_Status ss
){
#if TIME_STATISTICS
#endif
    Sos.status = ss;
}


static inline
void
System_marked(
    System_Status ss
){
    return ss == Sos.status;
}

#endif

#endif /* SOS_INC_SYSTEM_H_ */
