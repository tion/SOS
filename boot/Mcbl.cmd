#include "Memory.h"

-heap 0
-stack BL_STACK_SIZE

MEMORY {
	/* EMIF16 CE0 boot device */
	BOOT0:		o = BOOT_BASE		l = BOOT0_SZIE
	BOOT_R:		o = BOOT_RUN		l = BOOTR_SIZE
	LOADER_L:	o = LOADER_LOAD		l = LOADER_SZ

	MCBL_TLB:	o = MCBL_TLB_BASE	l = MCBL_TLB_SIZE
	UIMG:		o = UIMG_BASE 		l = UIMG_SIZE

	/* tail of 2MB shared L2, DONOT overlay */
	LOADER_R:	o = LOADER_RUN		l = LOADER_SZ

	/* tail of 512KB Local L2 SRAM, DONOT overlay */
	STACK:		o = BL_STACK		l = BL_STACK_SIZE
}

SECTIONS {
	.boot			>	BOOT0
	.ver			>	BOOT0
	.text			>	BOOT_R
	.cinit          >	BOOT_R
	.const          >	BOOT_R
	.switch         >	BOOT_R

	.neardata		>	BOOT_R
	.rodata			>	BOOT_R
	.rodata			>	BOOT_R
	.far            >	BOOT_R
	.fardata        >	BOOT_R
	.bss			>	LOADER_R
	.loader			:	LOAD = LOADER_L, RUN = LOADER_R

	.stack          >    STACK
}


