#ifndef SRC_CACUL_H_
#define SRC_CACUL_H_

#include <stdint.h>

typedef uint16_t Inum;

void CACUL_mmul(Inum tgt[], Inum n);
void CACUL_msub(Inum tgt[], Inum n);
void CACUL_madd(Inum tgt[], Inum n);
void CACUL_mdiv(Inum tgt[], Inum n);
void CACUL_numInit(Inum tgt[], Inum n);

#if CORE == 4
#define CACUL_e CACUL_mmul
#elif CORE == 5
#define CACUL_e CACUL_msub
#elif CORE == 6
#define CACUL_e CACUL_madd
#elif CORE == 7
#define CACUL_e CACUL_mdiv
#endif

#define CALCA_NUM   256
#define ALG_ROUND   4
typedef enum { PING = 0, PONG, PIONG } Piong;
typedef enum { MBASE = 0, MAMP, MMAX} Rtm;


#endif /* SRC_CACUL_H_ */
