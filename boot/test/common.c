#include <src/cacul.h>

void CACUL_numInit0(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n; /*i += 2*/) {
        Inum nxt = i + 1;
        tgt[i] = i;
        tgt[nxt] = nxt;
        i += 2;
    }
}

void CACUL_numInit(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n; i++) {
        tgt[i] = i;
    }
}
