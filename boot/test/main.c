#if defined(BOARD_INIT_667X_)
#include <Log.h>
#include <Device.h>
#endif
#include <src/cacul.h>


#pragma DATA_SECTION(caculNum, ".buf")
Inum caculNum[PIONG][ALG_ROUND][CALCA_NUM];

#pragma DATA_SECTION(cacul, ".shared")
struct {
	uint8_t inited[ALG_ROUND];
	uint8_t finished[ALG_ROUND];
	uint16_t cycleUse[ALG_ROUND][CALCA_NUM];
	uint8_t own;
	uint8_t round;
} cacul[PIONG];



int main(void) {
    #if defined(BOARD_INIT_667X_)
    Hw_socSetup((CorePac) 0);
    Log_dbg("\n\n[Hw]: SOC setup %x %s %f!\n\n", 0x1234, "hello", 3.1234);
    #endif


    CACUL_msub(caculNum[PING][3], CALCA_NUM);

	return caculNum[PING][3][0];
}
