#include <src/cacul.h>

#ifndef AMP_MODE
void madd(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n;)
        tgt[i] += i;
}
#else
void madd(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n;) {
        Inum nxt = i + 1;
        tgt[i] /= i;
        tgt[nxt] /= nxt;
    }
}
#endif
