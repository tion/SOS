#include <src/cacul.h>

#ifndef AMP_MODE
void CACUL_msub(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n;)
        tgt[i] -= i;
}
void CACUL_msub0(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n; /*n+=4*/) {
        tgt[i] -= i;

        tgt[i + 1] -= (i + 1);

//        Inum nxt2 = i + 2;
//        tgt[nxt2] -= nxt2;
//
//        Inum nxt3 = i + 3;
//        tgt[nxt3] -= nxt3;

        i += 2;
    }
}
#endif
