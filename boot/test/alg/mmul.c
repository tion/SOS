#include <src/cacul.h>

void mmul(Inum tgt[], Inum n) {
    for (Inum i = 0; i < n; i++)
        tgt[i] *= i;
}
