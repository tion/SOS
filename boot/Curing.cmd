#include "memspace.h"

-heap 0x1000
-stack BL_STACK_SIZE

MEMORY {
	/* tail of 2MB shared L2, DONOT overlay */
	CURING_R:	o = CURING_RUN	l = CURING_TOOL_SIZE

	/* tail of 512KB Local L2 SRAM, DONOT overlay */
	STACK:		o = BL_STACK	l = BL_STACK_SIZE
}

SECTIONS {
	.boot			>   CURING_R
	.loader			>   CURING_R
	.text           >   CURING_R
	.cinit          >   CURING_R
	.const          >   CURING_R
	.switch         >   CURING_R

	GROUP {
		.neardata
		.rodata
		.bss
		.cio
	} 				>    CURING_R
	.far            >    CURING_R
	.fardata        >    CURING_R
	.sysmem			>    CURING_R

	.stack          >    STACK
}


